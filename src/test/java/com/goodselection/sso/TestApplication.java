package com.goodselection.sso;

import com.goodselection.common.utils.GlobeWebFilter;
import com.goodselection.sso.web.TestController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.reactive.filter.OrderedWebFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.Collections;

/**
 * @author YaoXunYu
 * created on 12/17/2018
 */
@EnableGSSsoClient
@SpringBootApplication
@Import(TestController.class)
public class TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
        /*ConfigurableApplicationContext context = SpringApplication.run(TestApplication.class, args);
        SsoClientHandler ssoClientHandler = context.getBean(SsoClientHandler.class);
        ssoClientHandler.registerListener("1",
            (session, json) -> {
                System.out.println(json);
                return Mono.empty();
            });*/
    }

    @Bean
    public OrderedWebFilter globeWebFilter() {
        return new GlobeWebFilter(Integer.MIN_VALUE, Collections.emptySet());
    }
}
