package com.goodselection.sso.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author YaoXunYu
 * created on 12/27/2018
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/a")
    public String t() {
        return "HH";
    }
}
