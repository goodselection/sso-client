package com.goodselection.sso.test;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.goodselection.common.domains.user.UserRoles;
import com.goodselection.sso.client.connect.SsoClientHandler;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.netty.http.websocket.WebsocketInbound;

import java.net.URI;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author YaoXunYu
 * created on 12/17/2018
 */
public class Test {

    public static void main(String[] args) {
        SsoClientHandler handler = new SsoClientHandler();
        URI u = URI.create("ws://localhost:8080/");
        URI uri = u.resolve("/echo");
        System.out.println(uri);
        WebSocketClient webSocketClient = new ReactorNettyWebSocketClient();
        webSocketClient.execute(uri, handler)
          .block(Duration.ofSeconds(10));
        System.out.println("END");
    }

    private static HttpHeaders toHttpHeaders(WebsocketInbound inbound) {
        HttpHeaders headers = new HttpHeaders();
        io.netty.handler.codec.http.HttpHeaders nettyHeaders = inbound.headers();
        nettyHeaders.forEach(entry -> {
            String name = entry.getKey();
            headers.put(name, nettyHeaders.getAll(name));
        });
        return headers;
    }

    private static void jwtTest() throws InterruptedException {
        String token = generate();
        System.out.println(token);
        Thread.sleep(3000);
        DecodedJWT j = JWT.decode(token);
        System.out.println(j.getAlgorithm());
        DecodedJWT jwt = decode(token);
        jwt.getClaims()
          .forEach((k, v) -> {
              System.out.println("k: " + k);
              Object o = v.asString();
              if (o == null) {
                  o = v.asLong();
              }
              System.out.println("v: " + o);
          });
        System.out.println(ZonedDateTime.now().toEpochSecond());
    }

    private static String generate() {
        Algorithm algorithm = Algorithm.HMAC256("x");
        ZonedDateTime now = ZonedDateTime.now();
        UserRoles u = new UserRoles();
        u.setUserId(1L);
        u.setUsername("personA");
        u.addRole("roleA");
        String token = JWT.create()
          .withIssuedAt(Date.from(now.toInstant()))
          .withExpiresAt(Date.from(now.plusMinutes(1).toInstant()))
          //.withClaim("user_id", u.getUserId())
          //.withClaim("username", u.getUsername())
          .withClaim("user", u.toString())
          .withIssuer("auth0")
          .sign(algorithm);
        return token;
    }

    private static DecodedJWT decode(String token) {
        Algorithm algorithm = Algorithm.HMAC256("x");
        JWTVerifier.BaseVerification b = (JWTVerifier.BaseVerification) JWT.require(algorithm)
          .acceptIssuedAt(3L)
          .acceptExpiresAt(5L);
        JWTVerifier verifier = b.build(Date::new);
        DecodedJWT jwt = verifier.verify(token);
        String s = jwt.getClaim("user").asString();
        //jwt.getClaim()
        //User u = JSON.parseObject(s).toJavaObject(User.class);
        return jwt;
    }
}
