package com.goodselection.sso.support;

import com.goodselection.common.utils.RSAKeyBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.URI;
import java.security.interfaces.RSAPublicKey;

/**
 * @author YaoXunYu
 * created on 12/17/2018
 */
@ConfigurationProperties("com.goodselection.sso")
public class SsoProperties {
    private URI ssoServer;
    private URI appConn;
    private Integer appId;
    private String appName;
    private String appSecret;
    private URI userLogin;
    private String username;
    private String password;
    private JwtProperties jwt;

    public URI getSsoServer() {
        return ssoServer;
    }

    public void setSsoServer(String ssoServer) {
        this.ssoServer = URI.create(ssoServer);
    }

    public URI getAppConn() {
        return ssoServer.resolve(appConn);
    }

    public void setAppConn(String appConn) {
        this.appConn = URI.create(appConn);
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = Integer.valueOf(appId);
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public URI getUserLogin() {
        return ssoServer.resolve(userLogin);
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = URI.create(userLogin);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JwtProperties getJwt() {
        return jwt;
    }

    public void setJwt(JwtProperties jwt) {
        this.jwt = jwt;
    }

    public static class JwtProperties {
        private String issuer;

        private RSAPublicKey publicKey;

        public String getIssuer() {
            return issuer;
        }

        public void setIssuer(String issuer) {
            this.issuer = issuer;
        }

        public RSAPublicKey getPublicKey() {
            return publicKey;
        }

        public void setPublicKey(String publicKey) {
            this.publicKey = RSAKeyBuilder.buildPublic(publicKey,
              "can not init jwt user public key from properties");
        }
    }
}
