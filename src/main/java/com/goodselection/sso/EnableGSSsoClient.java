package com.goodselection.sso;

import com.goodselection.sso.client.authorization.JWTDecoder;
import com.goodselection.sso.client.authorization.PermissionRepository;
import com.goodselection.sso.client.authorization.SsoFilter;
import com.goodselection.sso.client.connect.*;
import com.goodselection.sso.support.SsoProperties;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author YaoXunYu
 * created on 12/17/2018
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({SsoProperties.class,
  SsoClient.class,
  SsoClientHandler.class,
  SsoEventRegister.class,
  SsoAuthorizationBuilder.class,
  SsoFilter.class,
  PermissionRepository.class,
  AppUserClient.class,
  JWTDecoder.class})
public @interface EnableGSSsoClient {
}
