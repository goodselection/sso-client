package com.goodselection.sso.client.authorization;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.goodselection.common.constants.SubscriberContextKeys;
import com.goodselection.common.domains.JWTServerWebExchange;
import com.goodselection.common.domains.auth.JWTUserAuthentication;
import com.goodselection.common.exceptions.AuthenticationException;
import com.goodselection.common.exceptions.AuthorizationException;
import com.goodselection.common.utils.AuthUtil;
import com.goodselection.sso.support.SsoProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.Set;

/**
 * @author YaoXunYu
 * created on 12/17/2018
 */
public class SsoFilter implements WebFilter {

    private final JWTDecoder jwtDecoder;
    private final PermissionRepository permissionRepository;
    private final WebClient webClient;

    @Autowired
    public SsoFilter(JWTDecoder jwtDecoder,
                     PermissionRepository permissionRepository,
                     SsoProperties ssoProperties) {
        this.jwtDecoder = jwtDecoder;
        this.permissionRepository = permissionRepository;
        this.webClient = WebClient.builder()
          .baseUrl(ssoProperties.getUserLogin().toString())
          .build();
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        String auth = AuthUtil.getAuth(exchange);
        return AuthUtil
          .authenticate(auth,
            a -> webClient.get()
              .header(HttpHeaders.AUTHORIZATION, a)
              .exchange()
              .map(clientResponse -> clientResponse.headers().asHttpHeaders().getFirst(HttpHeaders.AUTHORIZATION))
              .map(t -> jwtAuthenticate(exchange, t)),
            a -> Mono.just(jwtAuthenticate(exchange, a)))
          .flatMap(user -> doFilter(exchange, chain, user));
    }

    private JWTUserAuthentication jwtAuthenticate(ServerWebExchange exchange, String bearer) {
        ServerHttpRequest request = exchange.getRequest();
        JWTUserAuthentication user;
        try {
            String token = AuthUtil.bearerDecode(bearer);
            user = jwtDecoder.decode(token);
        } catch (JWTVerificationException e) {
            throw new AuthenticationException(e);
        }
        //authorize
        Set<String> roles = user.getRoles();
        RequestPath path = request.getPath();
        HttpMethod method = request.getMethod();
        permissionRepository.getPermissionOfRole(roles)
          .filter(permission -> permission.match(path, method))
          .findAny()
          .orElseThrow(AuthorizationException::new);
        return user;
    }

    private Mono<Void> doFilter(ServerWebExchange exchange, WebFilterChain chain, JWTUserAuthentication user) {
        return chain.filter(new JWTServerWebExchange(exchange, user))
          .subscriberContext(context ->
            context.put(SubscriberContextKeys.USER_AUTH, user));
    }
}
