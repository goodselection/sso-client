package com.goodselection.sso.client.authorization;

import com.goodselection.common.domains.permission.Permission;
import com.goodselection.common.domains.permission.Role;
import com.goodselection.common.domains.permission.RolePermissions;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

/**
 * @author YaoXunYu
 * created on 12/27/2018
 */
//TODO 改进数据存储结构
public class PermissionRepository {

    private ConcurrentHashMap<Integer, Role> roleMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Role> roleNameMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer, Permission> permissionMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer, ConcurrentHashMap.KeySetView<Integer, Boolean>> rpMap = new ConcurrentHashMap<>();

    public Stream<Permission> getPermissionOfRole(String role) {
        return Optional.ofNullable(roleNameMap.get(role))
          .filter(r -> !r.isFrozen())
          .map(Role::getRoleId)
          .flatMap(roleId -> Optional.ofNullable(rpMap.get(roleId)))
          .map(permissionIds -> permissionIds
            .stream()
            .map(permissionId -> permissionMap.get(permissionId))
            .filter(Objects::nonNull)
            .filter(permission -> !permission.isFrozen()))
          .orElseGet(Stream::empty);
    }

    public Stream<Permission> getPermissionOfRole(Collection<String> roles) {
        return roles.stream()
          .map(role -> roleNameMap.get(role))
          .filter(Objects::nonNull)
          .filter(role -> !role.isFrozen())
          .map(Role::getRoleId)
          .flatMap(roleId -> rpMap
            .get(roleId)
            .stream())
          .map(permissionId -> permissionMap.get(permissionId))
          .filter(Objects::nonNull)
          .filter(permission -> !permission.isFrozen());
    }

    public void replaceAll(List<RolePermissions> list) {
        rpMap.clear();
        roleNameMap.clear();
        permissionMap.clear();
        list.forEach(rp -> {
            Integer roleId = rp.getRoleId();
            String roleName = rp.getRole();
            Role role = new Role();
            role.setRoleId(roleId);
            role.setRole(rp.getRole());
            role.setFrozen(rp.isFrozen());
            roleMap.put(roleId, role);
            roleNameMap.put(roleName, role);
            ConcurrentHashMap.KeySetView<Integer, Boolean> keySetView = ConcurrentHashMap.newKeySet();
            rp.getPermissions()
              .stream()
              .peek(p -> permissionMap.putIfAbsent(p.getPermissionId(), p))
              .map(Permission::getPermissionId)
              .forEach(keySetView::add);
            rpMap.put(roleId, keySetView);
        });
    }

    public void clear() {
        roleMap.clear();
        roleNameMap.clear();
        permissionMap.clear();
        rpMap.clear();
    }

    public void roleFrozenChange(Role role) {
        Optional.ofNullable(roleMap.get(role.getRoleId()))
          .ifPresent(r -> r.setFrozen(role.isFrozen()));
    }

    public void renameRole(Role role) {
        Role ov = roleMap.get(role.getRoleId());
        roleNameMap.remove(ov.getRole());
        ov.setRole(role.getRole());
        roleNameMap.put(ov.getRole(), ov);
    }

    public void putPermission(Permission permission) {
        permissionMap.put(permission.getPermissionId(), permission);
    }

    public void deletePermission(Integer permissionId) {
        permissionMap.remove(permissionId);
        rpMap.forEach((k, v) -> v.remove(permissionId));
    }

    public void permissionFrozenChange(Permission permission) {
        Optional.ofNullable(permissionMap.get(permission.getPermissionId()))
          .ifPresent(p -> p.setFrozen(permission.isFrozen()));
    }

    public void addRolePermission(RolePermissions rp) {
        Optional.ofNullable(rpMap.get(rp.getRoleId()))
          .ifPresent(keySetView -> rp.getPermissions()
            .stream()
            .map(Permission::getPermissionId)
            .forEach(keySetView::add));
    }

    public void deleteRolePermission(RolePermissions rp) {
        Optional.ofNullable(rpMap.get(rp.getRoleId()))
          .ifPresent(keySetView -> rp.getPermissions()
            .stream()
            .map(Permission::getPermissionId)
            .forEach(keySetView::remove));
    }

    public void replaceRolePermission(RolePermissions rp) {
        ConcurrentHashMap.KeySetView<Integer, Boolean> keySetView = ConcurrentHashMap.newKeySet();
        rp.getPermissions()
          .stream()
          .map(Permission::getPermissionId)
          .forEach(keySetView::add);
        rpMap.put(rp.getRoleId(), keySetView);
    }
}
