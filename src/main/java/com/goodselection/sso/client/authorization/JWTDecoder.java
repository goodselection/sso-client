package com.goodselection.sso.client.authorization;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import com.goodselection.common.constants.JWTConstants;
import com.goodselection.common.domains.auth.JWTUserAuthentication;
import com.goodselection.sso.support.SsoProperties;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.interfaces.RSAPrivateKey;

/**
 * @author YaoXunYu
 * created on 12/25/2018
 */
public class JWTDecoder {

    private final JWTVerifier userVerifier;

    @Autowired
    public JWTDecoder(SsoProperties ssoProperties) {
        Algorithm userAlgorithm = Algorithm.RSA256(
          (PublicKeyProvider) keyId -> ssoProperties.getJwt().getPublicKey());
        this.userVerifier = JWT.require(userAlgorithm)
          .withIssuer(ssoProperties.getJwt().getIssuer())
          .acceptIssuedAt(3L)
          .acceptExpiresAt(5L)
          .build();
    }

    public JWTUserAuthentication decode(String token) {
        DecodedJWT jwt = verify(token);
        JWTUserAuthentication user = new JWTUserAuthentication(token);
        user.setUserId(jwt.getClaim(JWTConstants.USER_ID).asLong());
        user.setUsername(jwt.getClaim(JWTConstants.USERNAME).asString());
        user.setRoles(jwt.getClaim(JWTConstants.ROLES).asList(String.class));
        return user;
    }

    public DecodedJWT verify(String token) {
        return userVerifier.verify(token);
    }

    public interface PublicKeyProvider extends RSAKeyProvider {

        @Override
        default RSAPrivateKey getPrivateKey() {
            return null;
        }

        @Override
        default String getPrivateKeyId() {
            return null;
        }
    }
}
