package com.goodselection.sso.client.connect;

import com.alibaba.fastjson.JSONObject;
import com.goodselection.common.constants.RRConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * @author YaoXunYu
 * created on 12/17/2018
 */
public class SsoClientHandler implements WebSocketHandler {
    private static final Logger logger = LoggerFactory.getLogger(SsoClientHandler.class);

    private final Map<String, BiFunction<WebSocketSession, JSONObject, Mono<Void>>> map = new HashMap<>();

    public void registerListener(String route, BiFunction<WebSocketSession, JSONObject, Mono<Void>> func) {
        map.put(route, func);
    }

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        return session.receive()
          .flatMap(message -> {
              JSONObject json = JSONObject.parseObject(message.getPayloadAsText());
              String route = json.getString(RRConstants.ROUTE);
              BiFunction<WebSocketSession, JSONObject, Mono<Void>> func = map.get(route);
              if (func != null) {
                  return func.apply(session, json);
              } else {
                  logger.error("can't found listener for route: {}", route);
                  return Mono.empty();
              }
          })
          .then();
    }
}
