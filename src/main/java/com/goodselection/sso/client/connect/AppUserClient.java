package com.goodselection.sso.client.connect;

import com.goodselection.common.utils.AuthUtil;
import com.goodselection.sso.client.authorization.JWTDecoder;
import com.goodselection.sso.support.SsoProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author YaoXunYu
 * created on 02/22/2019
 * 用户登录,获取token,用于app之间访问
 */
public class AppUserClient {

    private final JWTDecoder jwtDecoder;
    private WebClient client;
    private AtomicReference<String> userAuthReference = new AtomicReference<>();

    @Autowired
    public AppUserClient(JWTDecoder jwtDecoder,
                         SsoProperties ssoProperties) {
        this.jwtDecoder = jwtDecoder;
        String bearer = AuthUtil.basicEncode(ssoProperties.getUsername(), ssoProperties.getPassword());
        this.client = WebClient.builder()
          .baseUrl(ssoProperties.getUserLogin().toString())
          .defaultHeader(HttpHeaders.AUTHORIZATION, bearer)
          .build();
    }

    public Mono<String> getUserAuth() {
        String userAuth = userAuthReference.get();
        if (userAuth != null) {
            try {
                jwtDecoder.verify(userAuth);
                return Mono.just(userAuth);
            } catch (Exception ignore) {
            }
        }
        return client.get()
          .acceptCharset(StandardCharsets.UTF_8)
          .exchange()
          .map(response -> response.headers().asHttpHeaders().getFirst(HttpHeaders.AUTHORIZATION))
          /*.doOnNext(auth -> {
              try {
                  jwtDecoder.verify(auth);
              } catch (Exception e) {
                  throw new AuthenticationException(e);
              }
          })*/
          .doOnSuccess(auth -> userAuthReference.set(auth));
    }
}
