package com.goodselection.sso.client.connect;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.goodselection.sso.support.SsoProperties;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author YaoXunYu
 * created on 12/25/2018
 */
public class SsoAuthorizationBuilder {
    private final SsoProperties ssoProperties;
    private final Algorithm algorithm;

    @Autowired
    public SsoAuthorizationBuilder(SsoProperties ssoProperties) {
        this.ssoProperties = ssoProperties;
        this.algorithm = Algorithm.HMAC512(ssoProperties.getAppSecret());
    }

    public String build() {
        ZonedDateTime now = ZonedDateTime.now();
        return JWT.create()
          .withIssuer(ssoProperties.getAppId().toString())
          .withIssuedAt(Date.from(now.toInstant()))
          .withExpiresAt(Date.from(
            now.plusMinutes(3).toInstant()))
          //.withClaim(JWTConstants.APP_NAME, ssoProperties.getAppName())
          .sign(algorithm);
    }
}
