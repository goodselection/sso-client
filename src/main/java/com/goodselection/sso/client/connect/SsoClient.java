package com.goodselection.sso.client.connect;

import com.goodselection.sso.client.authorization.PermissionRepository;
import com.goodselection.sso.support.SsoProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.Duration;

/**
 * @author YaoXunYu
 * created on 12/17/2018
 */
public class SsoClient extends AbstractWsReconnectClient {

    private final SsoClientHandler ssoClientHandler;
    private final SsoAuthorizationBuilder ssoAuthorizationBuilder;
    private final PermissionRepository permissionRepository;
    private final URI appConn;

    @Autowired
    public SsoClient(SsoClientHandler ssoClientHandler,
                     SsoAuthorizationBuilder ssoAuthorizationBuilder,
                     PermissionRepository permissionRepository,
                     SsoProperties ssoProperties) {
        super(Duration.ofMinutes(1));
        this.ssoClientHandler = ssoClientHandler;
        this.ssoAuthorizationBuilder = ssoAuthorizationBuilder;
        this.permissionRepository = permissionRepository;
        this.appConn = ssoProperties.getAppConn();
        connect().subscribe();
    }

    @Override
    public Mono<Void> connect() {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        logger.info("trying to connect to sso server {}", appConn.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, ssoAuthorizationBuilder.build());
        return client.execute(appConn, headers, ssoClientHandler)
          .doOnTerminate(() -> {
              logger.warn("sso server {} disconnect", appConn.toString());
              permissionRepository.clear();
              super.reconnectProcessor.onNext(this);
          });
    }
}
