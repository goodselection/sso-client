package com.goodselection.sso.client.connect;

import com.goodselection.common.constants.RRConstants;
import com.goodselection.common.constants.RRMethod;
import com.goodselection.common.domains.permission.Permission;
import com.goodselection.common.domains.permission.Role;
import com.goodselection.common.domains.permission.RolePermissions;
import com.goodselection.sso.client.authorization.PermissionRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author YaoXunYu
 * created on 02/12/2019
 */
public class SsoEventRegister implements InitializingBean {

    private final SsoClientHandler ssoClientHandler;
    private final PermissionRepository permissionRepository;

    @Autowired
    public SsoEventRegister(SsoClientHandler ssoClientHandler,
                            PermissionRepository permissionRepository) {
        this.ssoClientHandler = ssoClientHandler;
        this.permissionRepository = permissionRepository;
    }

    @Override
    public void afterPropertiesSet() {
        ssoClientHandler.registerListener(RRConstants.RP,
          (session, json) -> {
              RRMethod m = json.getObject(RRConstants.METHOD, RRMethod.class);
              if (RRMethod.POST.equals(m)) {
                  List<RolePermissions> rpList = json.getJSONArray(RRConstants.RESOURCE)
                    .toJavaList(RolePermissions.class);
                  permissionRepository.replaceAll(rpList);
              }
              return Mono.empty();
          });
        ssoClientHandler.registerListener(RRConstants.ROLE_NAME,
          (session, json) -> {
              RRMethod m = json.getObject(RRConstants.METHOD, RRMethod.class);
              if (RRMethod.PUT.equals(m)) {
                  Role role = json.getJSONObject(RRConstants.RESOURCE)
                    .toJavaObject(Role.class);
                  permissionRepository.renameRole(role);
              }
              return Mono.empty();
          });
        ssoClientHandler.registerListener(RRConstants.ROLE_FROZEN,
          (session, json) -> {
              RRMethod m = json.getObject(RRConstants.METHOD, RRMethod.class);
              if (RRMethod.PUT.equals(m)) {
                  Role role = json.getJSONObject(RRConstants.RESOURCE)
                    .toJavaObject(Role.class);
                  permissionRepository.roleFrozenChange(role);
              }
              return Mono.empty();
          });
        ssoClientHandler.registerListener(RRConstants.PERMISSION,
          (session, json) -> {
              RRMethod m = json.getObject(RRConstants.METHOD, RRMethod.class);
              Permission p = json.getJSONObject(RRConstants.RESOURCE)
                .toJavaObject(Permission.class);
              if (RRMethod.PUT.equals(m)) {
                  permissionRepository.putPermission(p);
              } else if (RRMethod.DELETE.equals(m)) {
                  permissionRepository.deletePermission(p.getPermissionId());
              }
              return Mono.empty();
          });
        ssoClientHandler.registerListener(RRConstants.PERMISSION_FROZEN,
          (session, json) -> {
              RRMethod m = json.getObject(RRConstants.METHOD, RRMethod.class);
              if (RRMethod.PUT.equals(m)) {
                  Permission p = json.getJSONObject(RRConstants.RESOURCE)
                    .toJavaObject(Permission.class);
                  permissionRepository.permissionFrozenChange(p);
              }
              return Mono.empty();
          });
        ssoClientHandler.registerListener(RRConstants.ROLE_PERMISSION,
          (session, json) -> {
              RRMethod m = json.getObject(RRConstants.METHOD, RRMethod.class);
              RolePermissions rp = json.getJSONObject(RRConstants.RESOURCE)
                .toJavaObject(RolePermissions.class);
              if (RRMethod.PUT.equals(m)) {
                  permissionRepository.addRolePermission(rp);
              } else if (RRMethod.DELETE.equals(m)) {
                  permissionRepository.deleteRolePermission(rp);
              } else if (RRMethod.POST.equals(m)) {
                  permissionRepository.replaceRolePermission(rp);
              }
              return Mono.empty();
          });
    }
}
