package com.goodselection.sso.client.connect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;
import reactor.core.publisher.UnicastProcessor;

import java.net.ConnectException;
import java.time.Duration;

/**
 * @author YaoXunYu
 * created on 12/19/2018
 */
public abstract class AbstractWsReconnectClient {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected UnicastProcessor<AbstractWsReconnectClient> reconnectProcessor = UnicastProcessor.create();

    protected AbstractWsReconnectClient(Duration reconnectDuration) {
        reconnect(reconnectDuration);
    }

    public abstract Mono<Void> connect();

    private void reconnect(Duration duration) {
        reconnectProcessor.publish()
          .autoConnect()
          .delayElements(duration)
          .flatMap(AbstractWsReconnectClient::connect)
          .onErrorContinue(throwable -> true,
            (throwable, o) -> {
                if (throwable instanceof ConnectException) {
                    logger.warn(throwable.getMessage());
                } else {
                    logger.error("unexpected error occur during websocket reconnect");
                    logger.error(throwable.getMessage());
                }
            })
          .doOnTerminate(() -> logger.error("websocket reconnect processor terminate"))
          .subscribe();
    }
}
